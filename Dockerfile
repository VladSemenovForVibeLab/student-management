FROM maven:3.8.5-openjdk-17 AS build
WORKDIR /
COPY /src /src
COPY pom.xml /
RUN mvn -f /pom.xml clean package -DskipTests
LABEL authors="vladislav-semenov"

FROM openjdk:17-jdk-slim
WORKDIR /
COPY /src /src
COPY --from=build /target/*.jar student.jar
EXPOSE 8080
ENTRYPOINT ["java", "-jar", "student.jar"]
