package com.semenov.studentmanagment.serviceImpl;

import com.semenov.studentmanagment.dto.auth.JWTRequest;
import com.semenov.studentmanagment.dto.auth.JWTResponse;
import com.semenov.studentmanagment.model.User;
import com.semenov.studentmanagment.security.JwtTokenProvider;
import com.semenov.studentmanagment.service.AuthService;
import com.semenov.studentmanagment.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.stereotype.Service;

@Service
public class AuthServiceImpl implements AuthService {
    @Autowired
    private  AuthenticationManager authenticationManager;
    @Autowired
    private  UserService userService;
   @Autowired
    private  JwtTokenProvider jwtTokenProvider;
    @Override
    public JWTResponse login(JWTRequest loginRequest) {
        JWTResponse jwtResponse = new JWTResponse();
        authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(loginRequest.getUsername(),loginRequest.getPassword()));
        User user = userService.getByUsername(loginRequest.getUsername());
        jwtResponse.setId(user.getId());
        jwtResponse.setUsername(user.getUsername());
        jwtResponse.setAccessToken(jwtTokenProvider.createAccessToken(user.getId(),user.getUsername(),user.getRoles()));
        jwtResponse.setRefreshToken(jwtTokenProvider.createRefreshToken(user.getId(),user.getUsername()));
        return jwtResponse;
    }

    @Override
    public JWTResponse refresh(String refreshToken) {
        return jwtTokenProvider.refreshUserTokens(refreshToken);
    }


}
