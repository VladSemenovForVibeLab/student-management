package com.semenov.studentmanagment.serviceImpl;

import com.semenov.studentmanagment.dao.CurriculumRepository;
import com.semenov.studentmanagment.model.Curriculum;
import com.semenov.studentmanagment.service.CurriculumService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CurriculumServiceImpl implements CurriculumService {
    private final CurriculumRepository curriculumRepository;

    @Autowired
    public CurriculumServiceImpl(CurriculumRepository curriculumRepository) {
        this.curriculumRepository = curriculumRepository;
    }

    @Override
    public List<Curriculum> getAllCurricula() {
        return curriculumRepository.findAll();
    }

    @Override
    public Curriculum getCurriculumById(Long id) {
        return curriculumRepository.findById(id).orElse(null);
    }

    @Override
    public Curriculum createCurriculum(Curriculum curriculum) {
        return curriculumRepository.save(curriculum);
    }

    @Override
    public Curriculum updateCurriculum(Long id, Curriculum curriculum) {
        // Проверка существования учебного плана
        if (!curriculumRepository.existsById(id)) {
            return null;
        }
        curriculum.setId(id);
        return curriculumRepository.save(curriculum);
    }

    @Override
    public void deleteCurriculum(Long id) {
        curriculumRepository.deleteById(id);
    }
}
