package com.semenov.studentmanagment.serviceImpl;

import com.semenov.studentmanagment.dao.StudentDao;
import com.semenov.studentmanagment.model.Student;
import com.semenov.studentmanagment.response.BasicResponseMsg;
import com.semenov.studentmanagment.response.StudentRequestVo;
import com.semenov.studentmanagment.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class StudentServiceImpl implements StudentService {
    @Autowired
    private StudentDao studentDao;
    @Override
    public BasicResponseMsg saveStudent(StudentRequestVo studentRequestVo) {
        BasicResponseMsg response=new BasicResponseMsg();
        try{
            Student student = new Student();
            student.setAddress(studentRequestVo.getAddress());
            student.setAge(studentRequestVo.getAge());
            student.setMobileNumber(studentRequestVo.getMobileNumber());
            student.setName(studentRequestVo.getName());
            student.setRollNo(studentRequestVo.getRollNo());
            Student save = studentDao.save(student);

            response.setData(save.getId());
            response.setMessage("student saved");
            response.setStatus(200);
        } catch (Exception e){
            e.printStackTrace();
        }
        return response;
    }

    @Override
    public BasicResponseMsg getStudent(StudentRequestVo studentRequestVo) {
        BasicResponseMsg response = new BasicResponseMsg();
        try{
            List<Student> findAll = studentDao.findAll();
            if(!findAll.isEmpty())
            {
               response.setStatus(200);
               response.setMessage("data found");
               response.setData(findAll);
            }else {
                response.setStatus(400);
                response.setMessage("data not found");
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return response;
    }

    @Override
    public BasicResponseMsg updateStudent(StudentRequestVo studentRequestVo) {
        BasicResponseMsg response = new BasicResponseMsg();
        try {
            Optional<Student> findById = studentDao.findById(Long.valueOf(studentRequestVo.getId()));
            if(findById.isPresent()){
                Student student= findById.get();
                student.setName(studentRequestVo.getName());
                student.setRollNo(studentRequestVo.getRollNo());
                Student save = studentDao.save(student);

                response.setData(save.getId());
                response.setMessage("student update");
                response.setStatus(200);
            }else{
                response.setStatus(400);
                response.setMessage("data not found");
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void deleteStudent(Long id) {
         studentDao.deleteById(id);
    }

    public StudentServiceImpl() {
    }
}
