package com.semenov.studentmanagment.serviceImpl;

import com.semenov.studentmanagment.dao.MarkRepository;
import com.semenov.studentmanagment.model.Mark;
import com.semenov.studentmanagment.service.MarkService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MarkServiceImpl implements MarkService {
    private final MarkRepository markRepository;

    @Autowired
    public MarkServiceImpl(MarkRepository markRepository) {
        this.markRepository = markRepository;
    }
    @Override
    public List<Mark> getAllMarks() {
        return markRepository.findAll();
    }

    @Override
    public Mark getMarkById(Long id) {
        return markRepository.findById(id).orElse(null);
    }

    @Override
    public Mark createMark(Mark mark) {
        return markRepository.save(mark);
    }

    @Override
    public Mark updateMark(Long id, Mark mark) {
        // Проверка существования оценки
        if (!markRepository.existsById(id)) {
            return null;
        }
        mark.setId(id);
        return markRepository.save(mark);
    }

    @Override
    public void deleteMark(Long id) {
        markRepository.deleteById(id);
    }
}
