package com.semenov.studentmanagment.restController;

import com.semenov.studentmanagment.dto.auth.JWTRequest;
import com.semenov.studentmanagment.dto.auth.JWTResponse;
import com.semenov.studentmanagment.dto.user.UserDto;
import com.semenov.studentmanagment.dto.validation.OnCreate;
import com.semenov.studentmanagment.mappers.UserMapper;
import com.semenov.studentmanagment.model.User;
import com.semenov.studentmanagment.service.AuthService;
import com.semenov.studentmanagment.service.UserService;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/auth")
@Tag(name = "Authentication Controller",description = "Authentication API")
public class AuthController {
    @Autowired
    private  AuthService authService;
    @Autowired
    private  UserService userService;
    @PostMapping("/login")
    public JWTResponse login(@Validated @RequestBody JWTRequest loginRequest){
        return authService.login(loginRequest);
    }

    @PostMapping("/register")
    public User register(@Validated(OnCreate.class) @RequestBody User userDto){
        return userService.create(userDto);
    }

    @PostMapping("/refresh")
    public JWTResponse refresh(@RequestBody String refreshToken){
        return authService.refresh(refreshToken);
    }

}
