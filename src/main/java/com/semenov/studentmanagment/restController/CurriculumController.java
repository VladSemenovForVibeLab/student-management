package com.semenov.studentmanagment.restController;

import com.semenov.studentmanagment.model.Curriculum;
import com.semenov.studentmanagment.service.CurriculumService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/curricula")
@Tag(name = "Curriculum Controller",description = "Curriculum API")
public class CurriculumController {
    private final CurriculumService curriculumService;

    @Autowired
    public CurriculumController(CurriculumService curriculumService) {
        this.curriculumService = curriculumService;
    }

    @Operation(summary = "Get All")
    @GetMapping
    public List<Curriculum> getAllCurricula() {
        return curriculumService.getAllCurricula();
    }

    @Operation(summary = "Get By Id")
    @GetMapping("/{id}")
    public Curriculum getCurriculumById(@PathVariable Long id) {
        return curriculumService.getCurriculumById(id);
    }

    @Operation(summary = "Create")
    @PostMapping
    public Curriculum createCurriculum(@RequestBody Curriculum curriculum) {
        return curriculumService.createCurriculum(curriculum);
    }

    @Operation(summary = "Update")
    @PutMapping("/{id}")
    public Curriculum updateCurriculum(@PathVariable Long id, @RequestBody Curriculum curriculum) {
        return curriculumService.updateCurriculum(id, curriculum);
    }

    @Operation(summary = "Delete")
    @DeleteMapping("/{id}")
    public void deleteCurriculum(@PathVariable Long id) {
        curriculumService.deleteCurriculum(id);
    }
}
