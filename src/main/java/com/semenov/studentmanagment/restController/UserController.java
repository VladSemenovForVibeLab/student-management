package com.semenov.studentmanagment.restController;

import com.semenov.studentmanagment.dto.user.UserDto;
import com.semenov.studentmanagment.dto.validation.OnUpdate;
import com.semenov.studentmanagment.mappers.UserMapper;
import com.semenov.studentmanagment.model.User;
import com.semenov.studentmanagment.service.UserService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.config.Task;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/users")
@Tag(name = "User Controller",description = "User API")
public class UserController {
    @Autowired
    private  UserService userService;
    @PutMapping
    @PreAuthorize("@customSecurityExpression.canAccessUser(#dto.id)")
    @Operation(summary = "Update User")
    public User update(@Validated(OnUpdate.class) @RequestBody User dto){
        return userService.update(dto);
    }
    @GetMapping("/{id}")
    @PreAuthorize("@customSecurityExpression.canAccessUser(#id)")
    @Operation(summary = "Get UserDto by Id")
    public User getById(@PathVariable Long id){
        return  userService.getById(id);
    }
    @DeleteMapping("/{id}")
    @PreAuthorize("@customSecurityExpression.canAccessUser(#dto.id)")
    @Operation(summary = "Delete User by Id")
    public void deleteById(@PathVariable Long id){
        userService.delete(id);
    }
}
