package com.semenov.studentmanagment.restController;

import com.semenov.studentmanagment.response.BasicResponseMsg;
import com.semenov.studentmanagment.response.StudentRequestVo;
import com.semenov.studentmanagment.service.StudentService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/students")
@Tag(name = "Student Controller",description = "Student API")
public class StudentController {
    @Autowired
    private StudentService studentService;
    @PostMapping("/saveStudent")
    @Operation(summary = "Create")
    public ResponseEntity<BasicResponseMsg> saveStudent(@RequestBody StudentRequestVo studentRequestVo) {
        BasicResponseMsg response= new BasicResponseMsg();
        try {
            response= studentService.saveStudent(studentRequestVo);
        }catch (Exception e){
            e.printStackTrace();
        }
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
    @GetMapping("/getStudent")
    @Operation(summary = "Get All")
    public ResponseEntity<BasicResponseMsg> getStudent(@RequestBody StudentRequestVo studentRequestVo){
        BasicResponseMsg response = new BasicResponseMsg();
        try{
            response=studentService.getStudent(studentRequestVo);
        }catch (Exception e){
            e.printStackTrace();
        }
        return new ResponseEntity<>(response,HttpStatus.OK);
    }
    @PutMapping("/updateStudent")
    @Operation(summary = "Update")
    public ResponseEntity<BasicResponseMsg> updateStudent(@RequestBody StudentRequestVo studentRequestVo){
        BasicResponseMsg response = new BasicResponseMsg();
        try{
            response=studentService.updateStudent(studentRequestVo);
        }catch (Exception e){
            e.printStackTrace();
        }
        return new ResponseEntity<>(response,HttpStatus.OK);
    }
    @DeleteMapping("/deleteStudent/{id}")
    @Operation(summary = "Delete")
    public ResponseEntity<BasicResponseMsg> deleteStudent(@PathVariable Long id) {
        try {
            studentService.deleteStudent(id);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ResponseEntity<>(HttpStatus.OK);
    }

}
