package com.semenov.studentmanagment.restController;

import com.semenov.studentmanagment.model.Mark;
import com.semenov.studentmanagment.service.MarkService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/marks")
@Tag(name = "Mark Controller",description = "Mark API")
public class MarkController {
    private final MarkService markService;

    @Autowired
    public MarkController(MarkService markService) {
        this.markService = markService;
    }

    @GetMapping
    @Operation(summary = "Get All")
    public List<Mark> getAllMarks() {
        return markService.getAllMarks();
    }

    @GetMapping("/{id}")
    @Operation(summary = "Get By Id")
    public Mark getMarkById(@PathVariable Long id) {
        return markService.getMarkById(id);
    }

    @PostMapping
    @Operation(summary = "Create")
    public Mark createMark(@RequestBody Mark mark) {
        return markService.createMark(mark);
    }
    @PutMapping("/{id}")
    @Operation(summary = "Update")
    public Mark updateMark(@PathVariable Long id, @RequestBody Mark mark) {
        return markService.updateMark(id, mark);
    }

    @DeleteMapping("/{id}")
    @Operation(summary = "Delete")
    public void deleteMark(@PathVariable Long id) {
        markService.deleteMark(id);
    }

}
