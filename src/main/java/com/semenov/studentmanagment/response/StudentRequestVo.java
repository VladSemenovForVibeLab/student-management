package com.semenov.studentmanagment.response;

import com.semenov.studentmanagment.model.Mark;

import java.util.List;

public class StudentRequestVo {
    private String name;
    private String age;
    private String address;
    private String mobileNumber;
    private String rollNo;
    private String email;
    private String gender;
    private String course;
    private String department;
    private List<Mark> marks;
    private String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public StudentRequestVo(String name, String age, String address, String mobileNumber, String rollNo) {
        this.name = name;
        this.age = age;
        this.address = address;
        this.mobileNumber = mobileNumber;
        this.rollNo = rollNo;
    }

    public StudentRequestVo(String name, String age, String address, String mobileNumber, String rollNo, String email, String gender, String course, String department, List<Mark> marks) {
        this.name = name;
        this.age = age;
        this.address = address;
        this.mobileNumber = mobileNumber;
        this.rollNo = rollNo;
        this.email = email;
        this.gender = gender;
        this.course = course;
        this.department = department;
        this.marks = marks;
    }

    public StudentRequestVo() {
    }
    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getRollNo() {
        return rollNo;
    }

    public void setRollNo(String rollNo) {
        this.rollNo = rollNo;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getCourse() {
        return course;
    }

    public void setCourse(String course) {
        this.course = course;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public List<Mark> getMarks() {
        return marks;
    }

    public void setMarks(List<Mark> marks) {
        this.marks = marks;
    }
}
