package com.semenov.studentmanagment;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication
@EnableTransactionManagement
public class StudentManagmentApplication {
    public static void main(String[] args) {
        SpringApplication.run(StudentManagmentApplication.class, args);
    }
}

