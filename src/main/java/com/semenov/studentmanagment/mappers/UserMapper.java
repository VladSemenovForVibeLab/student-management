package com.semenov.studentmanagment.mappers;

import com.semenov.studentmanagment.dto.user.UserDto;
import com.semenov.studentmanagment.model.User;
import org.mapstruct.Mapper;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

@Mapper
@Component
public interface UserMapper extends Mappable<User, UserDto> {

}
