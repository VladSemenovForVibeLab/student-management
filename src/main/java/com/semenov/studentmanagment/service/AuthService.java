package com.semenov.studentmanagment.service;

import com.semenov.studentmanagment.dto.auth.JWTRequest;
import com.semenov.studentmanagment.dto.auth.JWTResponse;

public interface AuthService {
    JWTResponse login(JWTRequest loginRequest);

    JWTResponse refresh(String refreshToken);

}
