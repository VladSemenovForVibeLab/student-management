package com.semenov.studentmanagment.service;

import com.semenov.studentmanagment.response.BasicResponseMsg;
import com.semenov.studentmanagment.response.StudentRequestVo;
import org.springframework.stereotype.Service;

public interface StudentService {
    BasicResponseMsg saveStudent(StudentRequestVo studentRequestVo);

    BasicResponseMsg getStudent(StudentRequestVo studentRequestVo);

    BasicResponseMsg updateStudent(StudentRequestVo studentRequestVo);

    void deleteStudent(Long id);
}
