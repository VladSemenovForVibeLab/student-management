package com.semenov.studentmanagment.service;

import com.semenov.studentmanagment.model.User;

public interface UserService {
    User getById(Long id);

    User getByUsername(String username);

    User update(User user);

    User create(User user);
    void delete(Long id);

}
