package com.semenov.studentmanagment.service;

import com.semenov.studentmanagment.model.Mark;

import java.util.List;

public interface MarkService {
    List<Mark> getAllMarks();
    Mark getMarkById(Long id);
    Mark createMark(Mark mark);
    Mark updateMark(Long id, Mark mark);
    void deleteMark(Long id);
}
