package com.semenov.studentmanagment.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.persistence.*;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@Entity
@Table(name = "teacher")
@Schema(description = "TEACHER")
public class Teacher {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "teacher_id", nullable = false)
    @Schema(description = "Teacher Id",example = "1")
    private Long id;

    @Schema(description = "Teacher name",example = "Vlad Semenov")
    @Column(name = "name")
    private String name;

    @Schema(description = "Teacher email",example = "ooovladislavchik@gmail.com")
    @Column(name = "email")
    private String email;
    @Schema(description = "Teacher phone number",example = "89188888888")
    @Column(name = "phone_number")
    private String phoneNumber;

    @Column(name = "address")
    @Schema(description = "Teacher address",example = "street 15")
    private String address;

    @Schema(description = "Teacher position",example = "director")
    @Column(name = "position")
    private String position;

    @Schema(description = "Teacher subjects",example = "RUS")
    @ManyToMany
    @JoinTable(
            name = "teacher_subject",
            joinColumns = @JoinColumn(name = "teacher_id"),
            inverseJoinColumns = @JoinColumn(name = "subject_id")
    )
    private List<Subject> subjects;

    @Schema(description = "Teacher active",example = "true")
    @Column(name = "is_active")
    private boolean isActive;

    @Schema(description = "Teacher start date",example = "yyyy-MM-dd HH:mm")
    @Column(name = "start_date")
    private LocalDateTime startDate;

    public LocalDateTime getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDateTime startDate) {
        this.startDate = startDate;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    public List<Subject> getSubjects() {
        return subjects;
    }

    public void setSubjects(List<Subject> subjects) {
        this.subjects = subjects;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public Teacher(Long id, String name, String email, String phoneNumber, String address, String position, List<Subject> subjects, boolean isActive, LocalDateTime startDate) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.phoneNumber = phoneNumber;
        this.address = address;
        this.position = position;
        this.subjects = subjects;
        this.isActive = isActive;
        this.startDate = startDate;
    }

    public Teacher() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

}