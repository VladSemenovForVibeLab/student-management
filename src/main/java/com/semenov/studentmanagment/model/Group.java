package com.semenov.studentmanagment.model;

import jakarta.persistence.*;

import java.util.List;

@Entity
@Table(name = "student_group")
public class Group {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "group_id", nullable = false)
    private Long id;
    @Column(name = "group_number")
    private String groupNumber;
    @Column(name = "specialty")
    private String specialty;
    @OneToMany(mappedBy = "group")
    private List<Student> students;

    @Column(name = "year_created")
    private int yearCreated;

    @ManyToOne
    @JoinColumn(name = "curator_id")
    private Teacher curator;

    public Teacher getCurator() {
        return curator;
    }

    public void setCurator(Teacher curator) {
        this.curator = curator;
    }

    public List<Student> getStudents() {
        return students;
    }

    public void setStudents(List<Student> students) {
        this.students = students;
    }

    public int getYearCreated() {
        return yearCreated;
    }

    public void setYearCreated(int yearCreated) {
        this.yearCreated = yearCreated;
    }

    public Group() {
    }

    public String getSpecialty() {
        return specialty;
    }

    public void setSpecialty(String specialty) {
        this.specialty = specialty;
    }

    public String getGroupNumber() {
        return groupNumber;
    }

    public void setGroupNumber(String groupNumber) {
        this.groupNumber = groupNumber;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Group(Long id, String groupNumber, String specialty, List<Student> students, int yearCreated, Teacher curator) {
        this.id = id;
        this.groupNumber = groupNumber;
        this.specialty = specialty;
        this.students = students;
        this.yearCreated = yearCreated;
        this.curator = curator;
    }
}