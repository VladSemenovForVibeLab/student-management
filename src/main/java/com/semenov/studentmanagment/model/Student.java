package com.semenov.studentmanagment.model;

import jakarta.persistence.*;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import java.time.LocalDateTime;
import java.util.List;

@Entity
@Table(name = "student")
public class Student {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "student_id")
    private Long id;
    @ManyToOne
    @JoinColumn(name = "group_id")
    private Group group;

    public Group getGroup() {
        return group;
    }

    public void setGroup(Group group) {
        this.group = group;
    }

    public List<Mark> getMarkList() {
        return markList;
    }

    public void setMarkList(List<Mark> markList) {
        this.markList = markList;
    }

    @Column(name = "name")
    private String name;
    @Column(name = "age")
    private String age;
    @Column(name = "mobile_number")
    private String mobileNumber;
    @Column(name = "address")
    private String address;
    @Column(name = "roll_no")
    private String rollNo;
    @CreationTimestamp
    @Column(name = "created_date")
    private LocalDateTime createDate;
    @Column(name = "email")
    private String email;
    @Column(name = "gender")
    private String gender;
    @Column(name = "course")
    private String course;
    @Column(name = "department")
    private String department;
    @Column(name = "birthdate")
    private LocalDateTime birthDate;
    @Column(name = "isactive")
    private boolean isActive;
    @UpdateTimestamp
    @Column(name = "updateddate")
    private LocalDateTime updatedDate;
    @OneToMany(mappedBy = "student", cascade = CascadeType.ALL,orphanRemoval = true)
    private List<com.semenov.studentmanagment.model.Mark> markList;

    public Student(String name, String age, String mobileNumber, String address, String rollNo) {
        this.name = name;
        this.age = age;
        this.mobileNumber = mobileNumber;
        this.address = address;
        this.rollNo = rollNo;
    }

    public LocalDateTime getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(LocalDateTime updatedDate) {
        this.updatedDate = updatedDate;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    public LocalDateTime getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(LocalDateTime birthDate) {
        this.birthDate = birthDate;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getCourse() {
        return course;
    }

    public void setCourse(String course) {
        this.course = course;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }


    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Student() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getRollNo() {
        return rollNo;
    }

    public void setRollNo(String rollNo) {
        this.rollNo = rollNo;
    }

    public LocalDateTime getCreateDate() {
        return createDate;
    }

    public void setCreateDate(LocalDateTime createDate) {
        this.createDate = createDate;
    }

    public Student(Long id, String name, String age, String mobileNumber, String address, String rollNo, LocalDateTime createDate) {
        this.id = id;
        this.name = name;
        this.age = age;
        this.mobileNumber = mobileNumber;
        this.address = address;
        this.rollNo = rollNo;
        this.createDate = createDate;
    }

    public Student(Long id, String name, String age, String mobileNumber, String address, String rollNo, LocalDateTime createDate, String email, String gender, String course, String department, LocalDateTime birthDate, boolean isActive, LocalDateTime updatedDate, List<Mark> marks) {
        this.id = id;
        this.name = name;
        this.age = age;
        this.mobileNumber = mobileNumber;
        this.address = address;
        this.rollNo = rollNo;
        this.createDate = createDate;
        this.email = email;
        this.gender = gender;
        this.course = course;
        this.department = department;
        this.birthDate = birthDate;
        this.isActive = isActive;
        this.updatedDate = updatedDate;
        this.markList = marks;
    }
}
