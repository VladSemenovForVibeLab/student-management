package com.semenov.studentmanagment.dao;

import com.semenov.studentmanagment.model.Student;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;

@Repository
public interface StudentDao extends JpaRepository<Student,Long> {
    // Поиск студентов по имени
    List<Student> findByName(String name);

    // Поиск студентов по номеру мобильного телефона
    List<Student> findByMobileNumber(String mobileNumber);

    // Поиск студентов по адресу
    List<Student> findByAddress(String address);

    // Поиск студентов по номеру ролла
    List<Student> findByRollNo(String rollNo);

    // Поиск студентов по электронной почте
    List<Student> findByEmail(String email);

    // Поиск студентов по полу
    List<Student> findByGender(String gender);
    // Поиск студентов по курсу
    List<Student> findByCourse(String course);

    // Поиск студентов по отделу
    List<Student> findByDepartment(String department);

    // Поиск студентов по дате рождения
    List<Student> findByBirthDate(LocalDateTime birthDate);

    // Получение списка студентов, отсортированных по дате создания в убывающем порядке
    List<Student> findByOrderByCreateDateDesc();
}
