package com.semenov.studentmanagment.dao;

import com.semenov.studentmanagment.enums.Role;
import com.semenov.studentmanagment.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
    Optional<User> findByUsername(String username);
    Optional<User> findByNameAndRolesContaining(String name, Role role);
}