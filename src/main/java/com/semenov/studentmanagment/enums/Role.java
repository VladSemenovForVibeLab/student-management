package com.semenov.studentmanagment.enums;

public enum Role {
    ROLE_USER,
    ROLE_ADMIN;

    Role() {
    }
}

