package com.semenov.studentmanagment.dto.auth;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotNull;

@Schema(description = "Request for login")
public class JWTRequest {
    @Schema(description = "email",example = "vlad@gmail.com")
    @NotNull(message = "Username must be not null")
    private String username;
    @Schema(description = "password for request",example = "123")
    @NotNull(message = "Password must be not null")
    private String password;

    public JWTRequest(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public JWTRequest() {
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
